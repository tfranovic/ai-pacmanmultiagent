# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions, Actions
import random, util

from game import Agent
eps=1e-9
INF=999999

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.
    
      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.
        
        getAction chooses among the best options according to the evaluation function.
        
        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()
        
        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best
        
        "Add more of your code here if you want to"
        
        return legalMoves[chosenIndex]
    
    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.
        
        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.
        
        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.
        
        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        foodPos=newFood.asList()
        ghostPos=[x.getPosition() for x in newGhostStates]
        closestFoodDist=0.0
        if len(foodPos)>0:
            closestFood=foodPos[0]
            for f in foodPos:
                if manhattanDistance(newPos, f)<manhattanDistance(newPos, closestFood):
                    closestFood=f
            closestFoodDist=manhattanDistance(newPos, closestFood)
        closestGhostDist=0.0
        if len(ghostPos)>0:
            closestGhost=ghostPos[0]
            for g in ghostPos:
                if manhattanDistance(newPos, g)<manhattanDistance(newPos, closestGhost):
                    closestGhost=g
            closestGhostDist=manhattanDistance(newPos, closestGhost)
        "*** YOUR CODE HERE ***"
        return successorGameState.getScore() + 1.0/(closestFoodDist+eps) - 2.0/(closestGhostDist+eps)
    
def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.
    
      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()
    
class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.
    
      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.
    
      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """
    
    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)
        self.agentDirections={}
    
class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.
        
          Here are some method calls that might be useful when implementing minimax.
        
          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1
        
          Directions.STOP:
            The stop direction, which is always legal
        
          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action
        
          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        realDepth=gameState.getNumAgents()*self.depth
        legalActions=gameState.getLegalActions(0)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        nextStates=[gameState.generateSuccessor(0,action) for action in legalActions]
        stateValues=[self.MiniMax(newState,1,realDepth-1) for newState in nextStates] 
        bestAction=[]
        bestValue=-INF
        for i in range(0,len(stateValues)):
            if stateValues[i]>bestValue:
                bestAction=[]
                bestValue=stateValues[i]
            if stateValues[i]==bestValue:
                bestAction.append(legalActions[i])
        idx = random.randint(0,len(bestAction)-1)
        #print(stateValues)
        return bestAction[idx]
    def MiniMax(self,gameState,agentIdx,depth):
        if (gameState.isLose() or gameState.isWin() or depth==0): 
            return self.evaluationFunction(gameState)
        legalActions=gameState.getLegalActions(agentIdx)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        nextStates=[gameState.generateSuccessor(agentIdx,action) for action in legalActions]
        if (agentIdx==0):
            return max([self.MiniMax(newState,(agentIdx+1)%gameState.getNumAgents(),depth-1) for newState in nextStates])
        else:
            return min([self.MiniMax(newState,(agentIdx+1)%gameState.getNumAgents(),depth-1) for newState in nextStates])
    
class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        realDepth=gameState.getNumAgents()*self.depth
        legalActions=gameState.getLegalActions(0)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        alpha=-INF
        beta=INF
        bestAction=[]
        bestScore=-INF
        for action in legalActions:
            score2=self.AlphaBeta(gameState.generateSuccessor(0,action),1,realDepth-1, alpha, beta)
            if(score2>bestScore):
                bestAction=[action]
                bestScore=score2
            if(score2==bestScore):
                bestAction.append(action)
            if score2>=beta:
                return bestAction
            alpha=max(score2,alpha)
        #idx = random.randint(0,len(bestAction)-1)
        #print(stateValues)
        return bestAction[0]
    def AlphaBeta(self,gameState,agentIdx,depth,alpha,beta):
        if (gameState.isLose() or gameState.isWin() or depth==0):
            return self.evaluationFunction(gameState)
        legalActions=gameState.getLegalActions(agentIdx)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        nextStates=[gameState.generateSuccessor(agentIdx,action) for action in legalActions]
        if (agentIdx==0):
            score=-INF
            for newState in nextStates:
                score=max(score,self.AlphaBeta(newState,(agentIdx+1)%gameState.getNumAgents(),depth-1, alpha, beta))
                if score>=beta:
                    return score
                alpha=max(score,alpha)
        else:
            score=INF
            for newState in nextStates:
                score=min(score,self.AlphaBeta(newState,(agentIdx+1)%gameState.getNumAgents(),depth-1, alpha, beta))
                if score<=alpha:
                    return score
                beta=min(score,beta)
        return score
class ExpectimaxAgent(MultiAgentSearchAgent):
    """
    Your expectimax agent (question 4)
    """
    
    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction
        
          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        realDepth=gameState.getNumAgents()*self.depth
        legalActions=gameState.getLegalActions(0)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        nextStates=[gameState.generateSuccessor(0,action) for action in legalActions]
        stateValues=[self.ExpectiMax(newState,1,realDepth-1) for newState in nextStates] 
        bestAction=[]
        bestValue=-INF
        for i in range(0,len(stateValues)):
            if stateValues[i]>bestValue:
                bestAction=[]
                bestValue=stateValues[i]
            if stateValues[i]==bestValue:
                bestAction.append(legalActions[i])
        idx = random.randint(0,len(bestAction)-1)
        #print(stateValues)
        return bestAction[idx]
    
    def ExpectiMax(self,gameState,agentIdx,depth):
        if (gameState.isLose() or gameState.isWin() or depth==0): 
                return self.evaluationFunction(gameState)
        legalActions=gameState.getLegalActions(agentIdx)
        if Directions.STOP in legalActions: 
            legalActions.remove(Directions.STOP)
        nextStates=[gameState.generateSuccessor(agentIdx,action) for action in legalActions]
        if (agentIdx==0):
            return max([self.ExpectiMax(newState,(agentIdx+1)%gameState.getNumAgents(),depth-1) for newState in nextStates])
        else:
            coeffs=[0.1 for s in nextStates]
            bestIdx=0
            minDist=INF
            for i in range(0,len(nextStates)):
                newDist=manhattanDistance(nextStates[i].getPacmanState().getPosition(), nextStates[i].getGhostStates()[agentIdx-1].getPosition())
                if(newDist<minDist):
                    mindist=newDist
                    bestIdx=i
            coeffs[i]=1.0-(len(coeffs)-1)*0.1
            retSum=0.0
            for i in range(0,len(nextStates)):
                retSum+=coeffs[i]*self.ExpectiMax(nextStates[i],(agentIdx+1)%gameState.getNumAgents(),depth-1)
            return retSum
    
def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).
    
      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    try:
        betterEvaluationFunction.firstCalled 
    except AttributeError:
        betterEvaluationFunction.NumOrginal=currentGameState.getFood().count()
    if currentGameState.isLose():
        return -INF
    if currentGameState.isWin() : 
        return INF
    newPos = currentGameState.getPacmanState().getPosition()
    allFood = currentGameState.getFood().asList()
    closestFood = allFood[0]
    minD = manhattanDistance(closestFood, newPos)
    for food in allFood:
        tD = manhattanDistance(food, newPos)
        if tD==1 : 
            closestFood=food
            minD=tD
            break
        if (tD < minD):
            closestFood = food  
            minD = tD
    realFoodDistance = aStarDist(currentGameState, closestFood)
    
    gStates = currentGameState.getGhostStates()
    scaredGhosts=[g for g in gStates if g.scaredTimer>0]
    closestSGhost=None
    closestSGhostD=INF
    for sG in scaredGhosts:
        sGD=aStarDist(currentGameState,sG.getPosition())
        if closestSGhost==None or closestSGhostD<sGD:
            closestSGhost=sG
            closestSGhostD=sGD
    
    normalGhosts=[g for g in gStates if g.scaredTimer<=0]
    closestNGhost=None
    closestNGhostD=INF
    for nG in normalGhosts:
        nGD=aStarDist(currentGameState,nG.getPosition())
        if closestNGhost==None or closestNGhostD<nGD:
            closestNGhost=nG
            closestNGhostD=nGD
    wF, wSG, wNG = [1.0,2.0,-3.0]
    if (closestNGhostD==0):
        return -INF
    if (closestSGhostD==0):
        closestSGhostD=0.1
    if (closestNGhostD > 2):
        if closestSGhost!=None:
            if (closestSGhostD<closestSGhost.scaredTimer):
                wF, wSG, wNG = [1.0,15.0,-2.0]; 
            else:
                wF, wSG, wNG = [6.0,-1.0,-3.0];    
    else:
        wF, wSG, wNG = [1.0, 1.0, -15.0]; 
    if len(allFood) < 4:
        wF, wSG, wNG= [10.0, 1.0, -6.0];
    if len(allFood) < 2:
        wF, wSG, wNG= [12.0, 0.0, -4.0];
    returnScore=(wF/realFoodDistance)+(wSG/closestSGhostD)+(wNG/closestNGhostD)+currentGameState.getScore()
    betterEvaluationFunction.firstCalled=False;
    return returnScore

def aStarDist(gameState, targetPos):
    allActions=[Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]
    curState=startPosition = gameState.getPacmanPosition()
    q = util.PriorityQueue()
    v=set()
    q.push(tuple((startPosition,0)), manhattanDistance(startPosition,targetPos))
    v.add(startPosition)
    while not q.isEmpty():
        curState,curDist = q.pop()
        if (curState==targetPos):
            return curDist
        for newState in [(int(curState[0]+dx),int(curState[1]+dy)) for (dx,dy) in [Actions.directionToVector(action) for action in allActions]]:
            if ((not gameState.getWalls()[newState[0]][newState[1]]) and (newState not in v)):
                curDist2 = curDist + 1
                v.add(newState)
                q.push(tuple((newState,curDist2)), manhattanDistance(newState,targetPos)+curDist2)    
    return curDist

# Abbreviation
better = betterEvaluationFunction
    
class ContestAgent(MultiAgentSearchAgent):
    """
      Your agent for the mini-contest
    """
    
    def getAction(self, gameState):
        """
          Returns an action.  You can use any method you want and search to any depth you want.
          Just remember that the mini-contest is timed, so you have to trade off speed and computation.
        
          Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
          just make a beeline straight towards Pacman (or away from him if they're scared!)
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()